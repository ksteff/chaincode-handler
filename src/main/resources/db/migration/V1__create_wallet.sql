CREATE TABLE wallet (
  label text NOT NULL PRIMARY KEY,
  json text NOT NULL,
  private_key text NOT NULL
)