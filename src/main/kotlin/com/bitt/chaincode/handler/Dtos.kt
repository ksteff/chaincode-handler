package com.bitt.chaincode.handler

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Data transfer objects for requests/responses
 */
data class RegisterUserRequest(
  @JsonProperty("affiliation")
  val affiliation: String,

  @JsonProperty("username")
  val username: String
)

data class CreateWalletRequest(
  @JsonProperty("label")
  val label: String,
  @JsonProperty("owner_user_id")
  val ownerUserId: String,
  @JsonProperty("organization_id")
  val organizationId: String,
  @JsonProperty("created_by")
  val createdBy: String
)

data class WalletSearchRequest(
  @JsonProperty("as_user")
  val asUser: String,
  @JsonProperty("wallet_id")
  val walletId: String
)

