package com.bitt.chaincode.handler

import org.hyperledger.fabric.sdk.security.CryptoSuiteFactory
import org.hyperledger.fabric_ca.sdk.HFCAClient
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*

/**
 * Spring configuration for Hyperledger components
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "chaincode-handler")
data class ChainCodeHandlerProperties(
  val adminUsername: String,
  val adminPassword: String,
  val allowAllHostNames: Boolean,
  val caBaseUrl: String,
  val caCertPath: String,
  val channelName: String,
  val chainCodeId: String,
  val enrollmentRequestHost: String,
  val enrollmentRequestProfile: String,
  val mspId: String,
  val networkConfigPath: String
)

@Configuration
class HyperledgerConfig(val chainCodeHandlerProperties: ChainCodeHandlerProperties) {

  @Bean
  fun caClient(): HFCAClient {
    val props = Properties()
    props["pemFile"] = chainCodeHandlerProperties.caCertPath
    props["allowAllHostNames"] = chainCodeHandlerProperties.allowAllHostNames.toString()
    val caClient = HFCAClient.createNewInstance(chainCodeHandlerProperties.caBaseUrl, props)
    val cryptoSuite = CryptoSuiteFactory.getDefault().cryptoSuite
    caClient.cryptoSuite = cryptoSuite
    return caClient
  }

}
