package com.bitt.chaincode.handler

import org.hyperledger.fabric.gateway.Gateway
import org.hyperledger.fabric.gateway.Wallet
import org.hyperledger.fabric.sdk.Enrollment
import org.hyperledger.fabric.sdk.User
import org.hyperledger.fabric_ca.sdk.EnrollmentRequest
import org.hyperledger.fabric_ca.sdk.HFCAClient
import org.hyperledger.fabric_ca.sdk.RegistrationRequest
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.io.StringWriter
import java.nio.file.Paths
import java.security.PrivateKey
import javax.json.Json

/**
 * Rest Controller for all operations to interact with the eccb smart contracts and user management
 */
@RestController
class Controller(
  val wallet: Wallet,
  val caClient: HFCAClient,
  val handlerProperties: ChainCodeHandlerProperties
) {

  @PostMapping("/admins")
  fun enrollAdmin(): ResponseEntity<Any> {
    val username = handlerProperties.adminUsername
    if (wallet.exists(username)) {
      return ResponseEntity.badRequest().body("Admin already enrolled")
    }
    val enrollmentRequest = EnrollmentRequest()
    enrollmentRequest.addHost(handlerProperties.enrollmentRequestHost)
    enrollmentRequest.profile = handlerProperties.enrollmentRequestProfile
    val enrollment = caClient.enroll(username, handlerProperties.adminPassword, enrollmentRequest)
    val user = Wallet.Identity.createIdentity(handlerProperties.mspId, enrollment.cert, enrollment.key)
    wallet.put(username, user)
    return ResponseEntity.ok("Admin enrolled")
  }

  @PostMapping("/users")
  fun registerUser(@RequestBody registerUserRequest: RegisterUserRequest): ResponseEntity<Any> {
    val username = registerUserRequest.username
    val userAffiliation = registerUserRequest.affiliation
    if (wallet.exists(username)) {
      return ResponseEntity.badRequest().body("User: $username already exists")
    }
    val adminUsername = handlerProperties.adminUsername
    if (!wallet.exists(adminUsername)) {
      return ResponseEntity.badRequest().body("Admin user needs to be enrolled and added to the wallet first")
    }
    val adminIdentity = wallet.get(adminUsername)
    val admin = createAdminUser(adminIdentity, userAffiliation)
    val registrationRequest = RegistrationRequest(username).apply {
      affiliation = userAffiliation
      enrollmentID = username
    }
    val enrollmentSecret = caClient.register(registrationRequest, admin)
    val enrollment = caClient.enroll(username, enrollmentSecret)
    val user = Wallet.Identity.createIdentity(handlerProperties.mspId, enrollment.cert, enrollment.key)
    wallet.put(username, user)
    return ResponseEntity.ok(registerUserRequest)
  }

  @PostMapping("/wallets", produces = ["application/json"])
  fun createWallet(@RequestBody request: CreateWalletRequest): ResponseEntity<Any> {
    val json = Json.createObjectBuilder()
      .add("Label", request.label)
      .add("OwnerUserID", request.ownerUserId)
      .add("OrganizationID", request.organizationId)
      .add("CreatedBy", request.createdBy)
      .build()
    val writer = StringWriter()
    Json.createWriter(writer)
      .use {
        jw -> jw.writeObject(json)
      }
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.createdBy)
      .networkConfig(Paths.get(handlerProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork(handlerProperties.channelName)
        val contract = network.getContract(handlerProperties.chainCodeId)
        contract.evaluateTransaction(
          "createWallet",
          writer.toString()
        )
      }
    return ResponseEntity.ok(String(result))
  }

  @PostMapping("/walletSearch", produces = ["application/json"])
  fun status(@RequestBody request: WalletSearchRequest): ResponseEntity<Any> {
    val result = Gateway
      .createBuilder()
      .identity(wallet, request.asUser)
      .networkConfig(Paths.get(handlerProperties.networkConfigPath))
      .discovery(true)
      .connect()
      .use {
        val network = it.getNetwork(handlerProperties.channelName)
        val contract = network.getContract(handlerProperties.chainCodeId)
        contract.evaluateTransaction(
          "getWallet",
          request.walletId
        )
      }
    return ResponseEntity.ok(String(result))
  }

  private fun createAdminUser(adminIdentity: Wallet.Identity, affiliation: String): User {
    return object : User {
      override fun getEnrollment(): Enrollment = object : Enrollment {
        override fun getKey(): PrivateKey = adminIdentity.privateKey
        override fun getCert(): String = adminIdentity.certificate
      }

      override fun getName(): String = handlerProperties.adminUsername
      override fun getRoles(): MutableSet<String> = emptySet<String>().toMutableSet()
      override fun getAffiliation(): String = affiliation
      override fun getAccount(): String? = null
      override fun getMspId(): String = handlerProperties.mspId
    }
  }

}