package com.bitt.chaincode.handler

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

/**
 * Main entrypoint for Spring Boot Application
 */
@EnableConfigurationProperties(ChainCodeHandlerProperties::class)
@SpringBootApplication
class Application

fun main(args: Array<String>) {
  runApplication<Application>(*args)
}