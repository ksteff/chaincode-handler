# chaincode-handler #

A Spring Boot application that interacts with the Hyperledger Fabric
ECCB Network.  It includes a Wallet implementation that stores
credentials in a PostgreSQL database which uses JPA for data access.

## Building and running ##

Next you'll need to compile a Docker image for chaincode-handler using gradle.  Run the
following commands in your terminal:

```
./gradlew jibDockerBuild
docker-compose up -d
```

## Verify with Postman ##

There is a postman collection included in the chaincode-handler project which you
can import into postman.  There are the following scenarios that you can test:

* Enroll Admin
* Register User

Run through each request and make sure it returns without error.  Then adjust
the request bodies to register another user and try using that user id in other
requests as well.  Also try using a non-existent user id and see the errors
that come back.

## Verify data with psql ##

Install postgresql with:

```
brew install postgresql
``` 

and connect with:

```
psql -h localhost -U chaincode-user -d chaincode-db
```

using password `password`

Verify the database by running:

```
chaincode-db=# \d
chaincode-db=# select * from wallet;
```

To see the wallet data contained in the PostgreSQL database.
